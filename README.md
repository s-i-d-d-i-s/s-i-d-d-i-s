<div align="center">
<h1>Hi 👋, I'm Siddharth Singh (s59_60r)</h1>
</div>
<div align="center">
<h3 >Software Engineer at Google | Competitive Programmer </h3>
</div>

<div align="center">
<img src='https://c.tenor.com/Pb_fcyVGSUYAAAAC/google-logo.gif' width='300'>
</div>

<div align="center">
  <a href="https://github.com/s-i-d-d-i-s"> <img src="https://img.shields.io/badge/C++-lightblue.svg?style=for-the-badge&logo=cplusplus" alt="s-i-d-d-i-s" /> </a>
  <a href="https://github.com/s-i-d-d-i-s"> <img src="https://img.shields.io/badge/C-lightblue.svg?style=for-the-badge&logo=cplusplus" alt="s-i-d-d-i-s" /> </a>
  <a href="https://github.com/s-i-d-d-i-s"> <img src="https://img.shields.io/badge/Python-lightblue.svg?style=for-the-badge" alt="s-i-d-d-i-s" /> </a>
  <a href="https://github.com/s-i-d-d-i-s"> <img src="https://img.shields.io/badge/Java-lightblue.svg?style=for-the-badge" alt="s-i-d-d-i-s" /> </a>
  <a href="https://github.com/s-i-d-d-i-s"> <img src="https://img.shields.io/badge/Javascript-lightblue.svg?style=for-the-badge" alt="s-i-d-d-i-s" /> </a>
  <a href="https://github.com/s-i-d-d-i-s"> <img src="https://img.shields.io/badge/TypeScript-lightblue.svg?style=for-the-badge" alt="s-i-d-d-i-s" /> </a>
  <a href="https://github.com/s-i-d-d-i-s"> <img src="https://img.shields.io/badge/Angular-lightblue.svg?style=for-the-badge" alt="s-i-d-d-i-s" /> </a>
</div>


<hr> 

- 🔭 I’m currently working on **Full Stack Software Developement, CLI, Cloud Applications**

- 💬 Ask me about **C++, Java, Angular, Python, Spring, Algorithms**

- ⚡ I have a passion for inventions, I love to create apps based on fresh ideas.

- 👨‍💼I am the inventor of
  - [CodeX: Editor](https://codex-editor.netlify.app)
  - [ContestReminder](https://discord.gg/yWdAV7nFGd)
  - [Leetcode2IDE](https://leetcode2ide.netlify.app/)
  - [Sparky - The Codechef Bot](https://discord.gg/7vzwAye2kN)
  - [Macro+ Plus](https://macro-plus.herokuapp.com/)


<hr>

#### Not very active in Competitive Programming anymore (still very competitive 😉 ) , but if i get time or feeling, i may do some again 😄

<div align="center">
<h2> My Coding Handles </h2>
</div>
<div align="center">
<a href="https://codechef.com/users/s59_60r"><img src="https://img.shields.io/badge/Codechef-2122-yellow?style=for-the-badge"></a>
<a href="https://codeforces.com/profile/s59_60r"><img src="https://img.shields.io/badge/Codeforces-1796-rgb(0%2C0%2C255)?style=for-the-badge"></a>
<a href="https://atcoder.jp/users/s59_60r"><img src="https://img.shields.io/badge/Atcoder-1300-rgb(0%2C192%2C192)?style=for-the-badge"></a>
<a href="https://leetcode.com/retired_s59_60r/"><img src="https://img.shields.io/badge/Leetcode-2082-ff69b4?style=for-the-badge"></a>
<a href="https://www.hackerrank.com/s5960r"><img src="https://img.shields.io/badge/Hackerrank-1835-green?style=for-the-badge"></a>
</div>


<div align="center">
<h2> Academics, Freelance and Internships </h2>
</div>

<div align="center">
<span><img src="https://img.shields.io/badge/BIT_Mesra-BTECH_CSE-orange?style=for-the-badge"></span>
<span><img src="https://img.shields.io/badge/GPA-8.2/10-rgb(0%2C0%2C255)?style=for-the-badge"></span>
<span><img src="https://img.shields.io/badge/Rank_Opener_2018-cyan?style=for-the-badge"></span>
<span><img src="https://img.shields.io/badge/Recieved_Scholarship-pink?style=for-the-badge"></span>
</div>

<hr>


[![ ](https://img.shields.io/badge/Google-Sept_2022-c0c0c0.svg?&style=for-the-badge&logo=Google&logoColor=Blue)](https://www.google.com/)
- Working in CitC Team

[![ ](https://img.shields.io/badge/Morgan_Stanley-Jan_2022-%232C3454.svg?&style=for-the-badge&logo=Morgan_Stanley&logoColor=Blue)](https://www.morganstanley.com/)
- Worked in P&L Control Department

[![ ](https://img.shields.io/badge/HackerEarth-Apr_2021-%232C3454.svg?&style=for-the-badge&logo=HackerEarth&logoColor=Blue)](https://research.samsung.com/sri-b)
- I did some more freelancing at hackerearth, where i created coding problems for Hackerearth Library.

[![ ](https://img.shields.io/badge/Samsung-Feb_2021-blue?style=for-the-badge&logo=Samsung&logoColor=Blue)](https://research.samsung.com/sri-b)
- I did my Internship at Samsung R&D, where i worked on tasks based on machine learning, and computer vision.

[![ ](https://img.shields.io/badge/GeeksForGeeks-May_2020-Green?style=for-the-badge)](https://www.geeksforgeeks.org/)
- I did some freelancing at GeeksForGeeks, where i wrote a couple of articles on algorithmic topics like Convex Hull, Hamming Distance etc.


<div align="center">
<h2> Reach Me Through </h2>
</div>
<div align="center">
<a href="https://www.linkedin.com/in/siddharthsingh3099"><img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"></a>
<a href="mailto:contact@imsid.in"><img src="https://img.shields.io/badge/Contact-0077B5?style=for-the-badge&logo=gmail&logoColor=white"></a>

</div>


